data "ignition_file" "container_unit" {
  path = "${var.home}/.config/containers/systemd/pairdrop.container"
  content {
    content = templatefile("${path.module}/templates/pairdrop.container", {
      publish_host = var.publish_host
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  files = [
    data.ignition_file.container_unit.rendered,
  ]
}
