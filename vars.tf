variable "home" {
  type = string
}

variable "uid" {
  type = number
}

variable "gid" {
  type = number
}

variable "publish_host" {
  type = string
  default = "127.0.0.1"
}
